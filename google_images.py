#!/usr/bin/python2
# coding: utf8


import json
import os
import time
import requests
from PIL import Image
from StringIO import StringIO
from requests.exceptions import ConnectionError
import grab
import urlparse
import urllib

IMAGES_PER_PAGE = 100
SLEEP_AFTER_FAIL = 60  # in seconds
SLEEP_PER_REQUEST = 2


def save_images(img_list, start, limit, out_dir):
    image_index = start
    images_saved = 0
    for img_ref in img_list:
        url = img_ref
        try:
            image_r = requests.get(url, timeout=4)
        except Exception as e:
            print 'could not download %s' % url
            continue
        # Remove file-system path characters from name.
        # title = image_info['titleNoFormatting'].replace('/', '').replace('\\', '')

        img_name = 'img_{0:05d}.jpg'.format(image_index)
        img_path = os.path.join(out_dir, img_name)
        img_file = open(img_path, 'w')
        try:
            Image.open(StringIO(image_r.content)).save(img_file, 'JPEG')
            img_file.close()

            url_img_name = os.path.basename(url)
            print u'Image [{0}] {1} successfully saved.'.format(image_index, url_img_name)
            image_index += 1
            images_saved += 1
            # control maximum number of images
            if image_index >= limit:
                break
        except IOError, e:
            # Throw away some gifs...blegh.
            img_file.close()
            os.remove(img_path) if os.path.exists(img_path) else None
            url_img_name = os.path.basename(url)
            print u'could not save {0}.'.format(url_img_name)
            continue
    return images_saved


def google_images(g, query, max_results, out_dir):

    base_url = "https://www.google.ru/search?q=" + query + "&source=lnms&tbm=isch&tbs=isz:m"
    if max_results is not None:
        limit = max_results
    else:
        limit = IMAGES_PER_PAGE

    saved_number = 0
    images_to_load = limit - saved_number

    # do until required number of images are loaded
    page = 0
    while images_to_load > 0:
        if page > 0:
            page_str = str(page)
            start_offset = IMAGES_PER_PAGE * page
            start_offset_str = str(start_offset)
            page_url = base_url + "&ijn=" + page_str + "&start=" + start_offset_str + "&newwindow=1"
        else:
            page_url = base_url

        max_attempts = 5
        attempt = 1
        success = False
        while attempt <= max_attempts:
            try:
                print 'sleep..',
                time.sleep(SLEEP_PER_REQUEST)
                print 'awake.'
                g.go(page_url)
                xpath_img = '//div[@class="rg_di rg_el ivg-i"]/div[@class="rg_meta"]//text()'
                page_list = g.xpath_list(xpath_img)

                if len(page_list) == 0:
                    print 'stop loading because response is empty'
                    images_to_load = 0
                    break

                imgs_urls = map(lambda x: json.loads(x)['ou'], page_list)

                res = save_images(imgs_urls, saved_number, limit, out_dir)
                saved_number += res
                images_to_load = limit - saved_number
                page += 1
                success = True
                break
            except Exception as e:
                str_srr = 'error while searching in google.images.'
                print '{0}, query = "{1}", attempt = [{2}/{3}]'.format(str_srr, query, attempt, max_attempts)
                print 'sleep for some time...',
                time.sleep(SLEEP_AFTER_FAIL)
                print 'awake.'
                attempt += 1
        if not success:
            images_to_load = 0


def google_images_api(query, out_dir):

    base_url = 'https://ajax.googleapis.com/ajax/services/search/images?' \
               'v=1.0&q=' + query + '&start=%d&imgtype=face&imgsz=xlarge'

    start = 0  # Google's start query string parameter for pagination.
    image_index = 0
    while start < 60:  # Google will only return a max of 56 results.
        r = requests.get(base_url % start)
        for image_info in json.loads(r.text)['responseData']['results']:
            url = image_info['unescapedUrl']
            try:
                image_r = requests.get(url)
            except ConnectionError, e:
                print 'could not download %s' % url
                continue
            # Remove file-system path characters from name.
            # title = image_info['titleNoFormatting'].replace('/', '').replace('\\', '')

            img_name = 'img_{0:05d}.jpg'.format(image_index)
            img_path = os.path.join(out_dir, img_name)
            img_file = open(img_path, 'w')
            try:
                Image.open(StringIO(image_r.content)).save(img_file, 'JPEG')
                img_file.close()
                print u'Image {0}/{1} successfully saved.'.format(query, img_name)
                image_index += 1
            except IOError, e:
                # Throw away some gifs...blegh.
                img_file.close()
                os.remove(img_path) if os.path.exists(img_path) else None
                print 'Could not save %s.' % url
                continue

        # print start
        start += 4  # 4 images per page.

        # Be nice to Google and they'll be nice back :)
        time.sleep(2)

