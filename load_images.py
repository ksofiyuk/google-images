#!/usr/bin/python2
# coding: utf8

import os
from argparse import ArgumentParser, SUPPRESS
from google_images import *
import codecs
import shutil
from unidecode import unidecode
from grab import Grab, GrabNetworkError, GrabError, GrabTimeoutError, GrabMisuseError

MAX_IMAGES_PER_REQUEST = 1000


def arg_parse():
    parser = ArgumentParser(description='searching and loading images from google')
    # command line arguments
    add_arg = parser.add_argument
    # file with celebrities names
    add_arg('--search-list', type=str, required=True, help='file with queries')

    add_arg('--start-index', type=int, default=1, required=False, help='index to start from, default is 1')
    # directory to write results
    add_arg('--output-dir', type=str, required=True, help='output directory')

    # parse arguments
    args = parser.parse_args()
    return args


def read_search_list_file(path):
    with codecs.open(path, 'r', 'utf-8') as f:
        content = f.read().splitlines()
    return content


def load_images(search_queries, start_index, data_dir):
    g = Grab(timeout=10, connect_timeout=3)
    for index, query in enumerate(search_queries):
        try:
            # query = unidecode(query)
            name_underscore = query[:].replace(" ", "_")
            directory_name = u'{0:05d}_{1}'.format(index + start_index, name_underscore)
            out_dir = os.path.join(data_dir, directory_name)
        except Exception as e:
            print 'error: bad query'
            continue

        # create output directory
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)
        google_images(g, query, MAX_IMAGES_PER_REQUEST, out_dir)


if __name__ == '__main__':
    args = arg_parse()
    search_list_file = os.path.abspath(args.search_list)
    output_dir = os.path.abspath(args.output_dir)
    start_index = args.start_index - 1;

    try:
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
    except Exception as e:
        print 'failed to create output folder.'
        exit(-1)

    stars = []
    try:
        queries = read_search_list_file(search_list_file)
    except (IOError, OSError):
        print 'cannot read file with list of queries.'
        exit(-1)

    if start_index < 0 or start_index > len(queries):
        print 'wrong start-index.'
        exit(-1)
    if start_index >= 0:
        queries = queries[start_index:]

    try:
        load_images(queries, start_index, output_dir)
    except Exception as e:
        print 'failed to load images.'
        print(str(e))
        exit(-1)

